import React from 'react';
import ProjectSummary from './ProjectSummary'
import {Link} from 'react-router-dom'

const ProjectList = ({projects}) => {
    // const projects = props.projects
    let proj
    if (projects){
        proj = <div className="section project-list">
            { projects.map(project => {
                return (
                    <Link to={'/project/' + project.id} key={project.id} >
                        <ProjectSummary project={project}/>
                    </Link>
                )
            })}
        </div>
    } else {
        proj = <p>There's currently no project in store</p>
    }

    return proj
}
 
export default ProjectList;