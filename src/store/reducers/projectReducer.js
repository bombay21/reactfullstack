const initState = {
    projects: [
        {id: '1', title: 'The Podium', content: 'Reception venue during Ada\'s wedding'},
        {id: '2', title: 'The Zone Tech Park', content: 'Meetup venue for concatenate conf'},
        {id: '3', title: 'JKK Building', content: 'Popular assessment center in Lagos'}
    ]
}

const projectReducer = (state = initState, action) => {
    switch (action.type) {
        case 'CREATE_PROJECT':
            console.log('created project', action.project);
            return state;
        case 'CREATE_PROJECT_ERROR':
            console.log('created project error', action.err);
            return state;
        default:
            return state;
    }
}

export default projectReducer