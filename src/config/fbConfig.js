import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/firestore'

// Initialize Firebase
const config = {
    apiKey: "AIzaSyDfwWCUpmQ0ZdJeq_EQ2MOwlb9TyTHox7M",
    authDomain: "react-venue.firebaseapp.com",
    databaseURL: "https://react-venue.firebaseio.com",
    projectId: "react-venue",
    storageBucket: "react-venue.appspot.com",
    messagingSenderId: "165853684402"
  };
  firebase.initializeApp(config);
  firebase.firestore().settings({ timestampsInSnapshots: true })

  export default firebase;